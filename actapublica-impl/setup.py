from setuptools import setup, find_namespace_packages

setup(
    name='actapublica-impl',
    packages=find_namespace_packages(include=['actapublica.*'])
)
