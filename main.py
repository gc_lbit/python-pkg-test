from actapublica.core.person import person
from actapublica.api.redis import redis

if __name__ == '__main__':
    print(person, f'module: {person.__module__}')
    print(redis, f'module: {redis.__module__}')
