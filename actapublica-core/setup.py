from setuptools import setup, find_namespace_packages

setup(
    name='actapublica-core',
    packages=find_namespace_packages(include=['actapublica.*'])
)